<?php

use Illuminate\Database\Seeder;

class ItemsSeeder extends Seeder
{
    public function run()
    {
        DB::table('items')->truncate();

		Comment::create(array(
			'name'=>'Agung Setiawan',
			'description'=>'The awesomeness of Laravel'
		));

		Comment::create(array(
			'name'=>'Hauril Nisfari',
			'description'=>'PHP coding is fun again'
		));

		Comment::create(array(
			'name'=>'Akhtar',
			'description'=>'Great framework btw'
        ));
    }
}
