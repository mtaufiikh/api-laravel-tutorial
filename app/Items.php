<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $fillable = [
        'id_prodi', 'name', 'nim',
    ];
    protected $hidden = [

    ];

    public function prodi()
    {
        return $this->belongsTo('App\DaftarProdi', 'id_prodi');
    }
}
