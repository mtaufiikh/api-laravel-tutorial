<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaftarProdi extends Model
{
    protected $table="prodi";

    protected $fillable = [
        'prodi'
    ];

    public function items()
    {
        return $this->hasMany('App\Items', 'id');
    }

}
