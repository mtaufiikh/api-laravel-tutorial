<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Items;
use App\DaftarProdi;
use Validator;

class ItemsController extends Controller
{
    public function sendGetResponse($result, $message)
    {
    	$response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }

    public function sendPutResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message
        ];
        return response()->json($response, 200);
    }

    public function sendDeleteResponse($message)
    {
        $response = [
            'success' => true,
            'message' => $message
        ];
        return response()->json($response, 200);
    }

    public function sendError($error, $errorMessages = [], $code = 404)
{
    	$response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }

    public function listItems()
    {
        $dataMhs = Items::all(['name','nim']);
        return $this->sendGetResponse($dataMhs->toArray(), 'Data Mahasiswa Berhasil Terkirim.');
    }

    public function createItems(request $req){
        $input = $req->all();

        $validator = Validator::make($input, [
            'id_prodi' => 'required',
            'name' => 'required',
            'nim' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $dataNim = Items::where('nim', $req->nim)->first();

        if($dataNim != null){
            return $this->sendError('Nim Telah Terpakai');
        }else{
            $dataMhs = Items::create($input);
            return $this->sendGetResponse($dataMhs->toArray(), 'Data Mahasiswa Berhasil Tersimpan.');
        }
    }

    public function updateItems(request $req, $id){
        $name = $req->name;
        $nim = $req->nim;
        $prodi = $req->prodi;

        $items = Items::find($id);
        $items->name = $name;
        $items->nim = $nim;
        $items->prodi = $prodi;
        $items->save();

        $response = Items::find($id);
        return $this->sendPutResponse($response, "Data Mahasiswa Berhasil Diubah");
    }

    public function deleteItems($id){
        $items = Items::find($id);
        $items->delete();

        return $this->sendDeleteResponse("Data Mahasiswa Berhasil Dihapus");
    }

    public function createProdis(Request $req){
        $input = $req->all();

        $validator = Validator::make($input, [
            'prodi' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $prodi = DaftarProdi::where('prodi', $req->prodi)->first();

        if($prodi != null){
            return $this->sendError('Prodi Telah Terpakai');
        }else{
            $dataProdi = DaftarProdi::create($input);
            return $this->sendGetResponse($dataProdi->toArray(), 'Data Prodi Berhasil Tersimpan.');
        }
    }

    public function listProdies()
    {
        $dataProdi = DaftarProdi::all();
        return $this->sendGetResponse($dataProdi->toArray(), 'Data Prodi Berhasil Terkirim.');
    }

    public function listData(){
        $data = Items::with('prodi')->get();
        return $this->sendGetResponse($data->toArray(), 'Semua Data Berhasil Terkirim.');
    }
}
