
<?php
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/', function (Request $request) {
//     return $request->user();
// });

Route::get('items','ItemsController@listItems');
Route::post('items','ItemsController@createItems');
Route::put('/items/{id}','ItemsController@updateItems');
Route::delete('/items/{id}','ItemsController@deleteItems');

Route::get('prodis','ItemsController@listProdies');
Route::post('prodis','ItemsController@createProdis');

Route::get('datas','ItemsController@listData');
